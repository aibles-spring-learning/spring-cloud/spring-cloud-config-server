package org.leonard.spring.cloud.config.server.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.leonard.spring.cloud.config.server.dto.response.ApplicationConfigurationResponse;
import org.leonard.spring.cloud.config.server.facade.ApplicationConfigurationFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ApplicationConfigurationController {

  private final ApplicationConfigurationFacade appConfigFacade;

  @GetMapping("/{application}/{profile}")
  public ApplicationConfigurationResponse getApplicationConfiguration(
      @PathVariable("application") String application,
      @PathVariable("profile") String profile
  ) {
    log.info("(getApplicationConfiguration)application: {}, profile: {}", application, profile);
    return appConfigFacade.getApplicationConfiguration(application, profile);
  }

}
