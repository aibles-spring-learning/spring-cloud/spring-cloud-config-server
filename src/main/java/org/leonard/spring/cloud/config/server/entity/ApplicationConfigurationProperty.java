package org.leonard.spring.cloud.config.server.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(staticName = "of")
@Data
@Entity
@NoArgsConstructor
@Table(
    name = "application_configuration_property",
    indexes = {
        @Index(name = "UI_application_profile", columnList = "application, profile", unique = true)
    }
)
public class ApplicationConfigurationProperty {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String application;

  private String profile;

  @Column(name = "property_key")
  private String key;

  @Column(name = "property_value")
  private String value;

}
