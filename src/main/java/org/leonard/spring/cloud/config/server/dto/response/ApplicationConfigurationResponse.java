package org.leonard.spring.cloud.config.server.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ApplicationConfigurationResponse {
  private String name;
  private List<String> profiles;
  private List<ApplicationConfigurationProperties> propertySources;
}
