package org.leonard.spring.cloud.config.server.service;

import org.leonard.spring.cloud.config.server.entity.ApplicationConfigurationProperty;

import java.util.List;

public interface ApplicationConfigurationPropertyService {
  List<ApplicationConfigurationProperty> list(String application, String profile);
}
