package org.leonard.spring.cloud.config.server.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class ApplicationConfigurationProperties {
  private String name;
  private Map<String, String> source;
}
