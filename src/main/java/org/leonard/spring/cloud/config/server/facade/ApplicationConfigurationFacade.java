package org.leonard.spring.cloud.config.server.facade;

import org.leonard.spring.cloud.config.server.dto.response.ApplicationConfigurationResponse;

public interface ApplicationConfigurationFacade {
  ApplicationConfigurationResponse getApplicationConfiguration(String application, String profile);
}
