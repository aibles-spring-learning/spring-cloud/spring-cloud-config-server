package org.leonard.spring.cloud.config.server.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.leonard.spring.cloud.config.server.entity.ApplicationConfigurationProperty;
import org.leonard.spring.cloud.config.server.repository.ApplicationConfigurationPropertyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ApplicationConfigurationPropertyServiceImpl implements ApplicationConfigurationPropertyService {

  private final ApplicationConfigurationPropertyRepository repository;

  @Override
  public List<ApplicationConfigurationProperty> list(String application, String profile) {
    log.info("(list)application: {}, profile: {}", application, profile);
    return repository.findByApplicationAndProfile(application, profile);
  }
}
