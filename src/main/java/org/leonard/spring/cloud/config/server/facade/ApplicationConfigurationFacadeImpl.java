package org.leonard.spring.cloud.config.server.facade;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.leonard.spring.cloud.config.server.dto.response.ApplicationConfigurationProperties;
import org.leonard.spring.cloud.config.server.dto.response.ApplicationConfigurationResponse;
import org.leonard.spring.cloud.config.server.service.ApplicationConfigurationPropertyService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class ApplicationConfigurationFacadeImpl implements ApplicationConfigurationFacade {

  private final ApplicationConfigurationPropertyService applicationConfigurationPropertyService;

  @Override
  public ApplicationConfigurationResponse getApplicationConfiguration(String application, String profile) {
    log.info("(getApplicationConfiguration)application: {}, profile: {}", application, profile);
    var response = new ApplicationConfigurationResponse();
    response.setName(application);
    response.setProfiles(List.of(profile));
    response.setPropertySources(getAppConfigPropertySources(application, profile));
    return response;
  }

  private List<ApplicationConfigurationProperties> getAppConfigPropertySources(String application, String profile) {
    var appConfigProperty = new ApplicationConfigurationProperties();
    appConfigProperty.setName(application + ".yml");
    appConfigProperty.setSource(getAppConfigPropertySource(application, profile));
    return List.of(appConfigProperty);
  }

  private Map<String, String> getAppConfigPropertySource(String application, String profile) {
    var appConfigPropertySource = new HashMap<String, String>();
    applicationConfigurationPropertyService.list(application, profile)
        .forEach(property -> appConfigPropertySource.put(property.getKey(), property.getValue()));
    return appConfigPropertySource;
  }
}
