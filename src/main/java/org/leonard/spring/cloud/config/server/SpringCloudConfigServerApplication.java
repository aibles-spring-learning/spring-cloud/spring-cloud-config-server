package org.leonard.spring.cloud.config.server;

import org.leonard.spring.cloud.config.server.entity.ApplicationConfigurationProperty;
import org.leonard.spring.cloud.config.server.repository.ApplicationConfigurationPropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class SpringCloudConfigServerApplication implements CommandLineRunner {

	@Autowired
	private ApplicationConfigurationPropertyRepository appConfigPropertyRepository;
	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfigServerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		appConfigPropertyRepository.saveAll(
				List.of(
						ApplicationConfigurationProperty.of(
								null, "config-client", "dev", "hello.user", "leonard-dev"
						),
						ApplicationConfigurationProperty.of(
								null, "config-client", "prod", "hello.user", "leonard-product"
						),
						ApplicationConfigurationProperty.of(
								null, "config-client", "default", "hello.user", "leonard-default"
						)
				)
		);
	}
}
