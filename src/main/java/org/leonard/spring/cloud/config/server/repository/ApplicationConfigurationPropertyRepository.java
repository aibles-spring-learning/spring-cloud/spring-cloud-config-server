package org.leonard.spring.cloud.config.server.repository;

import org.leonard.spring.cloud.config.server.entity.ApplicationConfigurationProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationConfigurationPropertyRepository extends JpaRepository<ApplicationConfigurationProperty, Long> {
  List<ApplicationConfigurationProperty> findByApplicationAndProfile(String application, String profile);
}
